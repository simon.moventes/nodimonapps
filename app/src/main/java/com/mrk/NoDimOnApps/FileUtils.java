package com.mrk.NoDimOnApps;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

public class FileUtils {

	private static final String DEBUG_FILE = Environment.getExternalStorageDirectory().getAbsolutePath() + "/mrktimeout.log";
	
	private static SharedPreferences preference;
	
	public static void Debug(Context context, String TAG, String log) {
		if (preference == null) preference = PreferenceManager.getDefaultSharedPreferences(context);
		
		if (preference.getBoolean("debug", false)) {
			try {
				PrintWriter writer = new PrintWriter(new FileWriter(DEBUG_FILE, true));

				writer.print(new SimpleDateFormat("dd/MM/yy HH:mm:ss z", Locale.FRANCE).format(new Date()));
				writer.print(" ");
				writer.print(TAG);
				writer.print(" ");
				writer.print(Thread.currentThread().getName());
				writer.print(" - ");
				writer.println(log);

				writer.flush();
				writer.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void Debug(Context context, String TAG, Exception exception) {
		if (preference == null) preference = PreferenceManager.getDefaultSharedPreferences(context);
		
		if (preference.getBoolean("debug", false)) {
			PrintWriter writer;
			try {
				writer = new PrintWriter(new FileWriter(DEBUG_FILE, true));
				exception.printStackTrace(writer);
				writer.flush();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
