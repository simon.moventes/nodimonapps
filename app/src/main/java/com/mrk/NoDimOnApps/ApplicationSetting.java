package com.mrk.NoDimOnApps;


public class ApplicationSetting {
	private String title = null;
	private String m_package = null;
	private String m_process = null;
	private int batteryScreenTimeOut = -2;
	private int acScreenTimeOut = -2;
	private boolean managed = false;

	public ApplicationSetting() {
	}

	public ApplicationSetting(String appTitle, String appPackage, int batTimeOut, int acTimeOut, String procName) {
		this.title = appTitle;
		this.m_package = appPackage;
		this.m_process = procName;
		this.batteryScreenTimeOut = batTimeOut;
		this.acScreenTimeOut = acTimeOut;
	}

	public boolean equals(ApplicationSetting app) {
		return (this.getProcess().equalsIgnoreCase(app.getProcess()) && 
				this.getPackage().equalsIgnoreCase(app.getPackage()));
	}

	public String getTitle() {
		return title;
	}

	public String getPackage() {
		return m_package;
	}

	public String getProcess() {
		return m_process;
	}

	public int getBatteryScreenTimeOut() {
		return batteryScreenTimeOut;
	}

	public int getACScreenTimeOut() {
		return acScreenTimeOut;
	}

	public boolean isManaged() {
		return managed;
	}

	public void setTitle(String tl) {
		title = tl;
	}

	public void setPackage(String pk) {
		m_package = pk;
	}

	public void setProcess(String pr) {
		m_process = pr;
	}

	public void setBatteryScreenTimeOut(int to) {
		batteryScreenTimeOut = to;
	}

	public void setACScreenTimeOut(int to) {
		acScreenTimeOut = to;
	}

	public void setManaged(boolean mg) {
		managed = mg;
	}
}