package com.mrk.NoDimOnApps;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class Preferences extends PreferenceActivity implements OnPreferenceClickListener {	

	private Context context;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Intent intent = new Intent(this, NoDimOnAppsService.class);
		stopService(intent);
		
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preference);
		
		context = this;
		PreferenceManager.setDefaultValues(this, R.xml.preference, false);
		
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		preferences.edit().putBoolean("verbose", preferences.getBoolean("verbose", false)).commit();
		
		Preference importPref = (Preference)findPreference("import_option");
		Preference exportPref = (Preference)findPreference("export_option");
		
		importPref.setOnPreferenceClickListener(this);
		exportPref.setOnPreferenceClickListener(this);
		
		hideDebugOption();
	}
	
	@SuppressWarnings("deprecation")
	private void hideDebugOption() {
		if (!BuildConfig.DEBUG) {
			CheckBoxPreference mCheckBoxPref = (CheckBoxPreference) findPreference("debug");
			PreferenceCategory mCategory = (PreferenceCategory) findPreference("Preferences");
			mCategory.removePreference(mCheckBoxPref);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		Intent intent = new Intent(this, NoDimOnAppsService.class);
		startService(intent);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Intent intent = new Intent(this, NoDimOnAppsService.class);
		stopService(intent);
	}
	
	@SuppressLint("SdCardPath") 
	@Override
    public boolean onPreferenceClick(Preference pref) {
		String orig = "/data/data/" + context.getPackageName() + "/files/" + "preferences.csv";
		String dest = Environment.getExternalStorageDirectory().getAbsolutePath() + "/preferences.csv";
    	if (pref.getKey().equalsIgnoreCase("import_option")) {
    		if (copy(dest, orig))
    			Toast.makeText(getApplication(), getResources().getString(R.string.import_ok), Toast.LENGTH_LONG).show();
    		else
    			Toast.makeText(getApplication(), getResources().getString(R.string.import_nok), Toast.LENGTH_LONG).show();
    	}
    	if (pref.getKey().equalsIgnoreCase("export_option")) {
    		if (copy(orig, dest))
    			Toast.makeText(getApplication(), getResources().getString(R.string.export_ok), Toast.LENGTH_LONG).show();
    		else
    			Toast.makeText(getApplication(), getResources().getString(R.string.export_nok), Toast.LENGTH_LONG).show();
    	}
    	return true;
    }
	
	private boolean copy(String src, String dst) {
		boolean ok = true;
		try {
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dst);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			ok = false;
		} catch (IOException e) {
			e.printStackTrace();
			ok = false;
		}

		return ok;
	}

}
