package com.mrk.NoDimOnApps;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

public class NoDimOnAppsActivity extends ListActivity {

	private Context context;
	private List<ResolveInfo> list;
	private SettingsManager managedApps;
	private ListAdapter adapter;
	private SlideViewAdapter slideAdapter;
	private PackageManager packageManager;
	private ReadAppsList readAppsList;

	private MenuItem searchMenu;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (Build.VERSION.SDK_INT > 20) {
			Window localWindow = getWindow();
			localWindow.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			localWindow.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			localWindow.setStatusBarColor(getResources().getColor(R.color.dark_blue));
		}

		context = this;

		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		setContentView(R.layout.list_layout);

		packageManager = getPackageManager();
	}

	@Override
	public void onStop() {
		super.onStop();

		managedApps.writeList();

		Intent intent = new Intent(this, NoDimOnAppsService.class);
		startService(intent);

		if (readAppsList != null)
			readAppsList.cancel(true);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart() {
		super.onStart();

		Intent intent = new Intent(this, NoDimOnAppsService.class);
		stopService(intent);

		init((RetainObject) getLastNonConfigurationInstance());
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);

		searchMenu = menu.findItem(R.id.search);
		SearchView searchView = (SearchView) searchMenu.getActionView();
		searchView.setOnQueryTextListener(searchListener);
		searchMenu.setVisible(false);

		int searchPlateId = searchView.getContext().getResources().getIdentifier("android:id/search_plate", null, null);
		View searchPlate = searchView.findViewById(searchPlateId);
		if (searchPlate != null) {
			int searchTextId = searchPlate.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
			TextView searchText = (TextView) searchPlate.findViewById(searchTextId);
			if (searchText != null) {
				searchText.setTextColor(getResources().getColor(R.color.white));
				searchText.setHintTextColor(getResources().getColor(R.color.white));
			}
		}

		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.options:
			Intent intent = new Intent(NoDimOnAppsActivity.this, Preferences.class);
			startActivity(intent);
			break;
		}
		return true;
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		final RetainObject retainObject = new RetainObject(managedApps, searchMenu);
		return retainObject;
	}

	private class RetainObject {
		public SettingsManager managedApps;
		public MenuItem searchMenu;

		public RetainObject(SettingsManager managedApps, MenuItem searchMenu) {
			this.managedApps = managedApps;
			this.searchMenu = searchMenu;
		}
	}

	private void init(RetainObject retainObject) {
		if (retainObject == null) {
			managedApps = new SettingsManager(context);
		} else {
			managedApps = retainObject.managedApps;
			searchMenu = retainObject.searchMenu;
		}

		adapter = new ListAdapter(context, R.layout.item_layout, managedApps) {
			public void updated() {
				adapter.notifyDataSetChanged();
			};
		};

		slideAdapter = new SlideViewAdapter(context, adapter, R.id.expandable_toggle_button, R.id.expandable, managedApps) {
			public void updated() {
				adapter.notifyDataSetChanged();
			};
		};
		setListAdapter(slideAdapter);

		if (retainObject == null) {
			setProgressBarIndeterminateVisibility(true);

			managedApps.setLoading(true);

			readAppsList = new ReadAppsList();
			readAppsList.execute();
		} else {
			for (ApplicationSetting setting : managedApps.getAppsSettingsList())
				adapter.add(setting);

			loadingEnd();
		}
	}

	private OnQueryTextListener searchListener = new OnQueryTextListener() {

		@Override
		public boolean onQueryTextChange(String text) {
			if (text.length() == 0) {
				setSelection(0);
			} else {
				for (int i = 0; i < managedApps.getAppsSettingsList().size(); i++) {
					if (managedApps.getAppsSettingsList().get(i).getTitle().toLowerCase(Locale.getDefault())
							.startsWith(text.toString().toLowerCase())) {
						setSelection(i);
						break;
					}
				}
			}
			return false;
		}

		@Override
		public boolean onQueryTextSubmit(String query) {
			return false;
		}

	};

	public class ReadAppsList extends AsyncTask<Void, ApplicationSetting, Void> {

		@Override
		protected void onPreExecute() {
			managedApps.clearList();

			ApplicationSetting defaultSetUp = managedApps.getDefault();
			defaultSetUp.setACScreenTimeOut(15);
			defaultSetUp.setBatteryScreenTimeOut(15);
			defaultSetUp.setManaged(true);

			managedApps.addToList(defaultSetUp, true);
		}

		@Override
		protected void onPostExecute(Void result) {
			managedApps.readList(false);

			loadingEnd();

			managedApps.setLoading(false);
		}

		@Override
		protected void onProgressUpdate(ApplicationSetting... progress) {
			managedApps.addToList(progress[0], true);
			adapter.notifyDataSetChanged();
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
			mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

			list = packageManager.queryIntentActivities(mainIntent, 0);

			for (ResolveInfo rInfo : list) {
				final ApplicationSetting application = new ApplicationSetting();

				application.setTitle(rInfo.activityInfo.loadLabel(packageManager).toString());
				application.setPackage(rInfo.activityInfo.packageName);
				application.setProcess(rInfo.activityInfo.name);

				publishProgress(application);
			}

			return null;
		}
	}

	private void loadingEnd() {
		setProgressBarIndeterminateVisibility(false);
		searchMenu.setVisible(true);

		sortList();

		adapter.notifyDataSetChanged();
	}

	private void sortList() {
		Comparator<ApplicationSetting> appsComparator = new Comparator<ApplicationSetting>() {
			public int compare(ApplicationSetting ap1, ApplicationSetting ap2) {
				if (ap1.getTitle().equals(context.getResources().getString(R.string.default_title)))
					return -1;
				else if (ap2.getTitle().equals(context.getResources().getString(R.string.default_title)))
					return 1;
				return ap1.getTitle().compareToIgnoreCase(ap2.getTitle());
			}
		};

		Collections.sort(managedApps.getAppsSettingsList(), appsComparator);
	}
}
