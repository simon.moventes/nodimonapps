package com.mrk.NoDimOnApps;

import java.lang.Thread.State;
import java.util.ArrayList;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Handler;

/**
 * This is an object that can load images from a URL on a thread.
 *
 * Modified version of snippet found at
 * https://ballardhack.wordpress.com/2010/04/10/loading-images-over-http-on-a-separate-thread-on-android/
 */
public class ImageThreadLoader {

	private final class QueueItem {
		public String appPackage;
		public ImageLoadedListener listener;
	}

	private final ArrayList<QueueItem> queue = new ArrayList<QueueItem>();
	private final Handler handler = new Handler(); // Assumes that this is started from the main (UI) thread
	private Thread thread;
	private QueueRunner runner = new QueueRunner();
	private Context context;

	/** Creates a new instance of the ImageThreadLoader */
	public ImageThreadLoader(Context context) {
		thread = new Thread(runner);
		this.context = context;
	}

	/**
	 * Defines an interface for a callback that will handle responses from the
	 * thread loader when an image is done being loaded.
	 */
	public interface ImageLoadedListener {
		public void imageLoaded(Drawable imageDrawable);
	}

	/**
	 * Provides a Runnable class to handle loading the image from the URL and
	 * settings the ImageView on the UI thread.
	 */
	private class QueueRunner implements Runnable {
		public void run() {
			synchronized (this) {
				while (queue.size() > 0) {
					final QueueItem item = queue.remove(0);
					
					if (item != null) {
						final Drawable bmp = readDrawableFromApplication(item.appPackage);
						if (bmp != null) {
							// Use a handler to get back onto the UI thread for the update
							handler.post(new Runnable() {
								public void run() {
									if (item.listener != null) {
										item.listener.imageLoaded(bmp);
									}
								}
							});
						}
					}
				}
			}
		}
	}

	/**
	 * Queues up a URI to load an image from for a given image view.
	 * @return A Drawable image if the image is in the cache, else null.
	 */
	public Drawable loadImage(final String appPackage, final ImageLoadedListener listener) {

		QueueItem item = new QueueItem();
		item.appPackage = appPackage;
		item.listener = listener;
		queue.add(item);

		// start the thread if needed
		if (thread.getState() == State.NEW) {
			thread.start();
		} else if (thread.getState() == State.TERMINATED) {
			thread = new Thread(runner);
			thread.start();
		}
		return null;
	}

	/**
	 * Reads and returns the application bitmap
	 * @param appPackage the application package to find
	 * @return the bitmap of the application
	 */
	public Drawable readDrawableFromApplication(String appPackage) {
		Drawable icon = null;

		try {
			icon = context.getPackageManager().getApplicationIcon(appPackage);
		} catch (NameNotFoundException e) {
			icon = context.getResources().getDrawable(android.R.drawable.sym_def_app_icon);
			e.printStackTrace();
		}

		return icon;
	}

}