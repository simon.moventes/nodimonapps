package com.mrk.NoDimOnApps;

import android.content.Context;

public class SeekBarsHelper {
	
	public static int getTimeoutFromProgress(int progress) {
    	if (progress == 0) return 15;
    	if (progress == 1) return 30;
    	if (progress == 2) return 60;
    	if (progress == 3) return 120;
    	if (progress == 4) return 300;
    	if (progress == 5) return 1800;
    	
    	return 15;
    }
    
	public static int getProgressFromTimeout(int timeout) {
    	if (timeout == 15) return 0;
    	if (timeout == 30) return 1;
    	if (timeout == 60) return 2;
    	if (timeout == 120) return 3;
    	if (timeout == 300) return 4;
    	if (timeout == 1800) return 5;
    	
    	return 0;
    }
    
	public static String getDisplay(SettingsManager managedApps, boolean isDefault, boolean isManaged, int timeOut, Context context) {
		String display;
		
		if (isManaged) {
			if (!isDefault && timeOut == managedApps.getDefault().getBatteryScreenTimeOut())
				display = context.getResources().getString(R.string.default_string);
			else if (timeOut == -1) display = context.getResources().getString(R.string.always_on);
			else if (timeOut == -2 || timeOut == 0) display = context.getResources().getString(R.string.default_string);
			else if (timeOut < 60) {
				display = String.valueOf(timeOut) + " " + context.getResources().getString(R.string.seconds);
			} else {
				display = String.valueOf(timeOut / 60) + " " + context.getResources().getString(R.string.minutes);
			}
		} else {
			display = context.getResources().getString(R.string.default_string);
		}
		
		return display;
	}
}